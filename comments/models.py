from django.db import models
from django.contrib.auth.models import User
from comments.constants import Constants
from django.utils.crypto import get_random_string
import uuid

# Create your models here.

class Profile(models.Model):

	class Meta:
		app_label = 'comments'

	def __unicode__(self):
		return self.profile_id

	def __str__(self):
		return self.display_name

	profile_id = models.CharField(max_length=36, default=get_random_string(length=36), primary_key=True)
	display_name = models.CharField(max_length=36, null=True)
	icon_url = models.CharField(max_length=300, null=True)
	date_created = models.DateTimeField(auto_now_add=True)
	last_updated = models.DateTimeField(auto_now=True)



class Thread(models.Model):
	
	class Meta:
		app_label = 'comments'
		
	def __unicode__(self):
		return self.thread_id
	
	owner = models.ForeignKey(Profile, on_delete=models.CASCADE)
	thread_id = models.CharField(max_length=36, default=get_random_string(length=36), primary_key=True)
	date_created = models.DateTimeField(auto_now_add=True)
	last_updated = models.DateTimeField(auto_now=True)
	
	
	
class Comment(models.Model):
	
	class Meta:
		app_label = 'comments'
		
	def __unicode__(self):
		return self.comment_id

	def __str__(self):
		return self.content
	
	comment_id = models.CharField(max_length=36, default=get_random_string(length=36), primary_key=True)
	thread = models.ForeignKey(Thread, on_delete=models.CASCADE)
	parent = models.CharField(max_length=36, null=True)
	date_created = models.DateTimeField(auto_now_add=True)
	last_updated = models.DateTimeField(auto_now=True)
	modified = models.DateTimeField(null=True)
	content = models.TextField(null=True)
	creator = models.ForeignKey(Profile, on_delete=models.CASCADE)
	up_vote_count = models.IntegerField(default=0)
	state = models.IntegerField(choices=Constants.CONTENT_STATES, default=Constants.STATE_VISIBLE, db_index=True)
	file_mime_type = models.CharField(max_length=32, null=True)
	file_url = models.CharField(max_length=255, null=True)
	
	
	
class Vote(models.Model):
	
	class Meta:
		app_label = 'comments'
		
	def __unicode__(self):
		return self.vote_id
	
	vote_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	comment = models.ForeignKey(Comment, on_delete=models.CASCADE)
	voter = models.ForeignKey(Profile, on_delete=models.CASCADE)
	date_created = models.DateTimeField(auto_now_add=True)
	last_updated = models.DateTimeField(auto_now=True)


class Flag(models.Model):

	class Meta:
		app_label = 'comments'

	def __unicode__(self):
		return self.flag_id

	def __str__(self):
		return self.reason

	flag_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	reason = models.TextField(max_length=300, null=True)
	flagger = models.ForeignKey(Profile, on_delete=models.CASCADE)
	date_created = models.DateTimeField(auto_now_add=True)
	last_updated = models.DateTimeField(auto_now=True)
	comment = models.ForeignKey(Comment, on_delete=models.CASCADE)
	state = models.IntegerField(choices=Constants.FLAG_STATES, default=Constants.STATE_PENDING, db_index=True)

	def was_published_recently(self):
		return self.date_created >= timezone.now() - datetime.timedelta(days=1)

	def comment_text(self):
		return self.comment.content

	def commenter(self):
		return self.comment.creator
